from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///snippets.db'
db = SQLAlchemy(app)

# Modèle de base de données pour Snippet
class Snippet(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.Text, nullable=False)

# Route pour l'URL racine, affiche le template et les snippets récents
@app.route('/', methods=['GET'])
def index():
    snippets = Snippet.query.all()  # Récupérer tous les snippets depuis la base de données
    return render_template('index.html', snippets=snippets)

# Route pour créer un nouveau snippet via POST
@app.route('/snippets', methods=['POST'])
def create_snippet():
    content = request.form.get('content', None)
    
    if not content:
        return jsonify({'error': 'Content is required'}), 400

    snippet = Snippet(content=content)
    db.session.add(snippet)
    db.session.commit()
    
    return jsonify({'id': snippet.id, 'content': snippet.content}), 201

@app.route('/snippets/<int:snippet_id>', methods=['GET'])
def get_snippet(snippet_id):
    snippet = Snippet.query.get_or_404(snippet_id)  # Récupérer le snippet par son ID ou renvoyer une erreur 404 si non trouvé
    return jsonify({'id': snippet.id, 'content': snippet.content}), 200


if __name__ == '__main__':
    db.create_all()  
    app.run(debug=True, host='0.0.0.0', port=8080)
